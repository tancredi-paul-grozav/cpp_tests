C++ test projects

Most of the work is licensed under LGPL https://www.gnu.org/licenses/lgpl.txt.
But please check the license of each project/folder to see the exact license.
If no license is specified please contact me at paul(at)grozav(dot)info.
